// This code section emulates a nios environment

var nios = {
    timerPeriodMs: 10,
    framePeriodMs: 50
}

document.addEventListener("DOMContentLoaded", function () {
    init();
    setInterval(onTimerEvent, nios.timerPeriodMs);
})

function init() {
    document.addEventListener("keypress", function (event) {
        switch (event.key) {
            case " ":
                onKEY0Event();
                break;
            case "p":
                onKEY1Event();
                break;
            case "c":
                onCollisionEvent();
                break;
            case "n":
                onNewFrameEvent();
                break;
        }
    });

    //Temp
    setInterval(onNewFrameEvent, nios.framePeriodMs);

}