/**
 * ==================================== CONSTANT VARIABLES ====================================
 */

var State = {
    IDLE: 0,
    PLAYING: 1,
    PAUSE: 2,
    GAME_OVER: 3
}

var constants = {
    timeIdleAfterGameOver: 5000,
    cloudToCactusSpeedRatio: 5,
    floorLevel: 550,
    dino: {
        jump: {
            height: 200, // px
            durationMs: 1300 // ms
        },
        posX: 20,
        sizeX: 60,
        sizeY: 100
    },
    cactus: {
        sizeX: 30,
        sizeY: 120,
        minDistBetween: 400 // px
    },
    cloud: {
        sizeX: 140,
        sizeY: 30,
        minDistBetween: 300, //px
        averageY: 100,
        yDispersion: 200
    },
    game: {
        minSpeed: 100, // px/s
        maxSpeed: 320, // px/s
        speedRampDurationS: 120 // s
    },
    window: {
        width: 1200, // px
        height: 600 // px
    }
}

/**
 * ==================================== DYNAMIC VARIABLES ====================================
 */

var flags = {
    KEY0: false,
    KEY1: false,
    collision: false,
    newFrame: false
};

var current = {
    state: State.IDLE,
    timeMs: 0,//ms
    game_finish_date: 0,
    waiting_for_reset: false,
    dino: {
        jumping: false,
        jumpBeginDate: null
    },
    cacti: [
        { posX: 0, creationTimeMs: 0, visible: false },
        { posX: 0, creationTimeMs: 0, visible: false },
        { posX: 0, creationTimeMs: 0, visible: false }
    ],
    clouds: [
        { posX: 0, dY: 0, creationTimeMs: 0, visible: false },
        { posX: 0, dY: 0, creationTimeMs: 0, visible: false },
        { posX: 0, dY: 0, creationTimeMs: 0, visible: false }
    ]
}

/**
 * ==================================== FUNCTIONS CALLED BY THE NIOS FRAMEWORK ====================================
 */

// Called every time the KEY0 key is pressed (space bar)
function onKEY0Event() {
    if (!flags.KEY0) {
        flags.KEY0 = true;
    }
}

// Called every time the KEY1 key is pressed ("p" key)
function onKEY1Event() {
    if (!flags.KEY1) {
        flags.KEY1 = true;
    }
}

// Called every time a collision event is raised by the VHDL (actually "c" key)
function onCollisionEvent() {
    if (!flags.collision) {
        flags.collision = true;
    }
}

// Called every time an image sync event is raised by the VHDL (actually "n" key)
// For easy use: called every nios.framePeriodMs
function onNewFrameEvent() {
    if (!flags.newFrame) {
        flags.newFrame = true;
    }
}

// Called periodically by the timer (its period is nios.timerPeriodMs)
function onTimerEvent() {
    if (current.state == State.PLAYING || current.state == State.GAME_OVER) {
        current.timeMs += nios.timerPeriodMs;
    }
    displayCurrentState();
    if (flags.collision) {
        handleCollision();
        flags.collision = false;
    }
    if (flags.KEY0) {
        handleKEY0();
        flags.KEY0 = false;
    }
    if (flags.KEY1) {
        handleKEY1();
        flags.KEY1 = false;
    }
    if (flags.newFrame) {
        handleNewFrame();
        flags.newFrame = false;
    }
    if (current.waiting_for_reset && (current.timeMs - current.game_finish_date) > constants.timeIdleAfterGameOver) {
        transitionGameOverToIdle();
    }
}


/**
 * ==================================== STATES MANAGEMENT ====================================
 */

function transitionIdleToPlaying() {
    current.state = State.PLAYING;
    current.timeMs = 0;
}

function transitionPlayingToPause() {
    current.state = State.PAUSE;
}

function transitionPauseToPlaying() {
    current.state = State.PLAYING;
}

function transitionPlayingToGameOver() {
    current.state = State.GAME_OVER;
    current.game_finish_date = current.timeMs;
    current.waiting_for_reset = true;
    current.dino.jumping = false;
}

function transitionGameOverToPlaying() {
    current.state = State.PLAYING;
    current.waiting_for_reset = false;
    current.timeMs = 0;
}

function transitionGameOverToIdle() {
    current.state = State.IDLE;
    current.waiting_for_reset = false;
}

/**
 * ==================================== DISPLAY FUNCTIONS ====================================
 */

function displayCurrentState() {
    var str = "unknown";
    switch (current.state) {
        case State.IDLE:
            str = "IDLE";
            break;
        case State.PLAYING:
            str = "PLAYING";
            break;
        case State.PAUSE:
            str = "PAUSE";
            break;
        case State.GAME_OVER:
            str = "GAME_OVER";
            break;
    }
    document.getElementById("state").innerHTML = str;
}

function clearCanvas() {
    var c = document.getElementById("canvas");
    var ctx = c.getContext("2d");
    ctx.clearRect(0, 0, c.width, c.height);
}

function writeText(text) {
    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext("2d");
    ctx.font = "30px Arial";
    ctx.strokeText(text, 10, 50);
}

function drawHorizon() {
    var c = document.getElementById("canvas");
    var ctx = c.getContext("2d");
    ctx.beginPath();
    ctx.moveTo(0, constants.floorLevel - 25);
    ctx.lineTo(c.width, constants.floorLevel - 25);
    ctx.stroke();
}

function drawDino() {
    var y = 0;
    if (current.dino.jumping) {
        var timeSinceJumpBegin = current.timeMs - current.dino.jumpBeginDate;
        if (timeSinceJumpBegin < constants.dino.jump.durationMs) {
            // parabolic jump
            var A = -4 * constants.dino.jump.height / (constants.dino.jump.durationMs * constants.dino.jump.durationMs);
            var B = 4 * constants.dino.jump.height / constants.dino.jump.durationMs;
            y = A * timeSinceJumpBegin * timeSinceJumpBegin + B * timeSinceJumpBegin;
        } else {
            current.dino.jumping = false;
        }
    }
    var c = document.getElementById("canvas");
    var ctx = c.getContext("2d");
    ctx.fillStyle = "#FF0000";
    ctx.fillRect(constants.dino.posX, constants.floorLevel - constants.dino.sizeY - y, constants.dino.sizeX, constants.dino.sizeY);
}

function drawCacti() {
    current.cacti.forEach(cactus => {
        if (cactus.visible) {
            var c = document.getElementById("canvas");
            var ctx = c.getContext("2d");
            ctx.fillStyle = "#18AD65";
            ctx.fillRect(cactus.posX, constants.floorLevel - constants.cactus.sizeY, constants.cactus.sizeX, constants.cactus.sizeY);
        }
    });
}

function drawClouds() {
    current.clouds.forEach(cloud => {
        if (cloud.visible) {
            var c = document.getElementById("canvas");
            var ctx = c.getContext("2d");
            ctx.fillStyle = "#4ADBFF";
            ctx.fillRect(cloud.posX, constants.cloud.averageY + cloud.dY, constants.cloud.sizeX, constants.cloud.sizeY);
        }
    });
}

function displayScore() {
    document.getElementById("score").innerHTML = Math.floor(current.timeMs / 100);
}

/**
 * ==================================== CACTI MANAGEMENT FUNCTIONS ====================================
 */

function getFurthestCactusX() {
    var maxX = -1;
    current.cacti.forEach(cactus => {
        if (cactus.visible && cactus.posX > maxX) {
            maxX = cactus.posX;
        }
    });
    return maxX;
}

function getFreeCactusSlotIndex() {
    var i = 0;
    while (i < current.cacti.length) {
        if (!current.cacti[i].visible) {
            return i;
        }
        i++;
    }
    return -1;
}

function setCacti() {
    // shift all displayed cacti
    current.cacti.forEach(cactus => {
        if (cactus.visible) {
            cactus.posX = constants.window.width - (current.timeMs - cactus.creationTimeMs) / 1000 * getGameSpeed();
        }
    });

    // check cacti that exit the window
    current.cacti.forEach(cactus => {
        if (cactus.posX <= 0) {
            cactus.visible = false;
        }
    });

    // get a free slot to display a new cactus
    var freeSlotIndex = getFreeCactusSlotIndex();
    if (freeSlotIndex >= 0) { // If a slot is available
        var furthestX = getFurthestCactusX();
        if (furthestX == -1 /* no cactus displayed */ || (constants.window.width - furthestX) > constants.cactus.minDistBetween) {
            //TODO: aleat generation X
            current.cacti[freeSlotIndex].visible = true;
            current.cacti[freeSlotIndex].creationTimeMs = current.timeMs;
            current.cacti[freeSlotIndex].posX = constants.window.width - constants.cactus.sizeX;
        }
    }

    drawCacti();
}

/**
 * ==================================== CLOUDS MANAGEMENT FUNCTIONS ====================================
 */

function getFurthestCloudX() {
    var maxX = -1;
    current.clouds.forEach(cloud => {
        if (cloud.visible && cloud.posX > maxX) {
            maxX = cloud.posX;
        }
    });
    return maxX;
}

function getFreeCloudSlotIndex() {
    var i = 0;
    while (i < current.clouds.length) {
        if (!current.clouds[i].visible) {
            return i;
        }
        i++;
    }
    return -1;
}

function setClouds() {
    // shift all displayed clouds
    current.clouds.forEach(cloud => {
        if (cloud.visible) {
            cloud.posX = constants.window.width - (current.timeMs - cloud.creationTimeMs) / 1000 * getGameSpeed() / constants.cloudToCactusSpeedRatio;
        }
    });

    // check clouds that exit the window
    current.clouds.forEach(cloud => {
        if (cloud.posX <= 0) {
            cloud.visible = false;
        }
    });

    // get a free slot to display a new cactus
    var freeSlotIndex = getFreeCloudSlotIndex();
    if (freeSlotIndex >= 0) { // If a slot is available
        var furthestX = getFurthestCloudX();
        if (furthestX == -1 /* no cloud displayed */ || (constants.window.width - furthestX) > constants.cloud.minDistBetween) {
            //TODO: aleat generation X
            current.clouds[freeSlotIndex].visible = true;
            current.clouds[freeSlotIndex].dY = Math.round(Math.random() * constants.cloud.yDispersion - constants.cloud.yDispersion / 2);
            current.clouds[freeSlotIndex].creationTimeMs = current.timeMs;
            current.clouds[freeSlotIndex].posX = constants.window.width - constants.cloud.sizeX;
        }
    }

    drawClouds();
}

/**
 * ==================================== CUSTOM FUNCTIONS ====================================
 */

function handleCollision() {
    console.log("collision event");
    if (current.state == State.PLAYING) {
        transitionPlayingToGameOver();
    }
}

function handleNewFrame() {
    // console.log("new frame event");
    clearCanvas();
    if (current.state == State.PLAYING) {
        drawHorizon();
        setCacti();
        setClouds();
        drawDino();
        displayScore();
    }
    if (current.state == State.PAUSE) {
        writeText("pause");
    }
    if (current.state == State.GAME_OVER) {
        writeText("game over");
    }
    if (current.state == State.IDLE) {
        writeText("press jump to start");
    }
}

function handleKEY0() {
    console.log("KEY0 event (jump)");
    if (current.state == State.IDLE) {
        transitionIdleToPlaying();
    } else if (current.state == State.GAME_OVER) {
        transitionGameOverToPlaying();
    } else if (current.state == State.PLAYING) {
        jumpDino();
    }
}

function handleKEY1() {
    console.log("KEY1 event (pause)");
    if (current.state == State.PLAYING) {
        transitionPlayingToPause();
    } else if (current.state == State.PAUSE) {
        transitionPauseToPlaying();
    }
}

function jumpDino() {
    if (!current.dino.jumping) {
        current.dino.jumping = true;
        current.dino.jumpBeginDate = current.timeMs;
    }
}

/**
 * Returns a the current cacti shifting speed px/s
 */
function getGameSpeed() {
    var output;
    if (current.timeMs < constants.game.speedRampDurationS * 1000) {
        output = constants.game.minSpeed + (constants.game.maxSpeed - constants.game.minSpeed) / (constants.game.speedRampDurationS * 1000) * current.timeMs;
    } else {
        output = constants.game.maxSpeed;
    }
    return Math.round(output*100)/100;
}