# DINIOSAUR (NIOS II) INTERFACES

## Inputs

| name            | size | irq | description                                                      |
|-----------------|:----:|:---:|------------------------------------------------------------------|
| clk             |  1   |     | 50 MHz                                                           |
| image_sync      |  1   |  x  | '1' when a new frame is displayed                                |
| collision       |  1   |  x  | '1' when a collision is detected (dinosaur/cactus)               |
| keys            |  4   |  x  | input keys used by the player                                    |
| dino_icon_dim   |  20  |     | dino_icon_dim[19..10]: y size<br>dino_icon_dim[9..0]: x size     |
| cactus_icon_dim |  20  |     | cactus_icon_dim[19..10]: y size<br>cactus_icon_dim[9..0]: x size |
| cloud_icon_dim  |  20  |     | cloud_icon_dim[19..10]: y size<br>cloud_icon_dim[9..0]: x size   |

## Outupts

| count | name          | size | static | description                                                                                                |
|-------|---------------|------|--------|------------------------------------------------------------------------------------------------------------|
| 1     | dino_y        | 10   |        | dinosaur y location                                                                                        |
| 1     | dino_x        | 10   | x      | dinosaur x location                                                                                        |
| n     | cloud_n       | 21   |        | cloud[20]: '1' when displayed else '0'<br>cloud[19..10]: cloud x location<br>cloud[9..0]: cloud y location |
| n     | cactus_n      | 11   |        | catcus[10]: '1' when displayed else '0'<br>cactus[9..0]: cactus x location                                 |
| 1     | state         | 2    |        | current program state (idle: '0' , playing: '1', pause: '2', game_over: '3')                               |
| 1     | score         | 16   |        | current score (big endian)                                                                                 |
| 1     | life          | 3    |        | current life level                                                                                         |
| 1     | cheat_mod     | 1    |        | if '1' no collision detected else the collisions are handled                                               |
| 1     | horizon_level | 10   | x      | y location                                                                                                 |
| 1     | cactus_y      | 10   | x      | y location                                                                                                 |
| 1     | sky_color     | 24   | x      | sky_color[23..16]: R component<br>sky_color[15..8]: G component<br>sky_color[7..0]: B component            |
| 1     | ground_color  | 24   | x      | ground_color[23..16]: R component<br>ground_color[15..8]: G component<br>ground_color[7..0]: B component   |