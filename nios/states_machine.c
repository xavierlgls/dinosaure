#include <io.h>
#include "my_types.h"
#include "dynamic_var.h"
#include "configuration.h"
#include "system.h"

void transitionIdleToPlaying() {
	setState(PLAYING);
	disableCheatMod();
	ignoringCollisionUntilMs = 0;
	life = LIFE_LEVEL_INIT;
	displayLevel(life);
	timeMs = 0;
}

void transitionPlayingToPause() {
	setState(PAUSE);
	enableCollisionInterrupt(FALSE);
}

void transitionPauseToPlaying() {
	if(ignoringCollision){
		enableCollisionInterrupt(FALSE);
	}else{
		enableCollisionInterrupt(TRUE);
	}
	setState(PLAYING);
}

void transitionPlayingToGameOver() {
	life = LIFE_LEVEL_INIT;
	displayLevel(life);
	enableCollisionInterrupt(FALSE);
	setState(GAME_OVER);
	gameFinishDateMs = timeMs;
	waitingForReset = TRUE;
	dinoJumping = FALSE;
}

void transitionGameOverToPlaying() {
	setState(PLAYING);
	if(ignoringCollision){
		enableCollisionInterrupt(FALSE);
	}else{
		enableCollisionInterrupt(TRUE);
	}
	waitingForReset = FALSE;
	timeMs = 0;
}

void transitionGameOverToIdle() {
	setState(IDLE);
	waitingForReset = FALSE;
}

void setState(state nextState){
	IOWR(STATE_BASE, 0, nextState);
	currentState = nextState;
}
