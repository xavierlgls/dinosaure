#ifndef DISPLAY_H_
#define DISPLAY_H_

void jumpDino();
void updateDino();
void updateCacti();
void updateClouds();
void displayScore(int score);
void displayLevel(int value);

#endif /*DISPLAY_H_*/
