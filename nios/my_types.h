// --------------------------------------------------------------------
// Copyright (c) 2007 by Terasic Technologies Inc.
// --------------------------------------------------------------------
//
// Permission:
//
//   Terasic grants permission to use and modify this code for use
//   in synthesis for all Terasic Development Boards and Altera Development
//   Kits made by Terasic.  Other use of this code, including the selling
//   ,duplication, or modification of any portion is strictly prohibited.
//
// Disclaimer:
//
//   This VHDL/Verilog or C/C++ source code is intended as a design reference
//   which illustrates how these types of functions can be implemented.
//   It is the user's responsibility to verify their design for
//   consistency and functionality through the use of formal
//   verification methods.  Terasic provides no warranty regarding the use
//   or functionality of this code.
//
// --------------------------------------------------------------------
//
//                     Terasic Technologies Inc
//                     356 Fu-Shin E. Rd Sec. 1. JhuBei City,
//                     HsinChu County, Taiwan
//                     302
//
//                     web: http://www.terasic.com/
//                     email: support@terasic.com
//
// --------------------------------------------------------------------

#ifndef MY_TYPES_H_
#define MY_TYPES_H_

typedef int bool;
#define TRUE    1
#define FALSE   0

typedef int state;
#define IDLE 		0
#define PLAYING 	1
#define PAUSE 		2
#define GAME_OVER 	3

typedef struct cloud
{
    int posX;
    int posY;
    long creationTimeMs;
    bool visible;
} cloud;

typedef struct catcus
{
    int posX;
    long creationTimeMs;
    bool visible;
} cactus;



#endif /*MY_TYPES_H_*/
