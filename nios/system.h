/*
 * system.h - SOPC Builder system and BSP software package information
 *
 * Machine generated for CPU 'nios2_gen2_0' in SOPC Builder design 'diniosaur'
 * SOPC Builder design path: ../../diniosaur.sopcinfo
 *
 * Generated: Tue Nov 26 18:28:50 CET 2019
 */

/*
 * DO NOT MODIFY THIS FILE
 *
 * Changing this file will have subtle consequences
 * which will almost certainly lead to a nonfunctioning
 * system. If you do modify this file, be aware that your
 * changes will be overwritten and lost when this file
 * is generated again.
 *
 * DO NOT MODIFY THIS FILE
 */

/*
 * License Agreement
 *
 * Copyright (c) 2008
 * Altera Corporation, San Jose, California, USA.
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * This agreement shall be governed in all respects by the laws of the State
 * of California and by the laws of the United States of America.
 */

#ifndef __SYSTEM_H_
#define __SYSTEM_H_

/* Include definitions from linker script generator */
#include "linker.h"


/*
 * CPU configuration
 *
 */

#define ALT_CPU_ARCHITECTURE "altera_nios2_gen2"
#define ALT_CPU_BIG_ENDIAN 0
#define ALT_CPU_BREAK_ADDR 0x00200820
#define ALT_CPU_CPU_ARCH_NIOS2_R1
#define ALT_CPU_CPU_FREQ 50000000u
#define ALT_CPU_CPU_ID_SIZE 1
#define ALT_CPU_CPU_ID_VALUE 0x00000000
#define ALT_CPU_CPU_IMPLEMENTATION "fast"
#define ALT_CPU_DATA_ADDR_WIDTH 0x16
#define ALT_CPU_DCACHE_BYPASS_MASK 0x80000000
#define ALT_CPU_DCACHE_LINE_SIZE 32
#define ALT_CPU_DCACHE_LINE_SIZE_LOG2 5
#define ALT_CPU_DCACHE_SIZE 2048
#define ALT_CPU_EXCEPTION_ADDR 0x00100020
#define ALT_CPU_FLASH_ACCELERATOR_LINES 0
#define ALT_CPU_FLASH_ACCELERATOR_LINE_SIZE 0
#define ALT_CPU_FLUSHDA_SUPPORTED
#define ALT_CPU_FREQ 50000000
#define ALT_CPU_HARDWARE_DIVIDE_PRESENT 0
#define ALT_CPU_HARDWARE_MULTIPLY_PRESENT 1
#define ALT_CPU_HARDWARE_MULX_PRESENT 0
#define ALT_CPU_HAS_DEBUG_CORE 1
#define ALT_CPU_HAS_DEBUG_STUB
#define ALT_CPU_HAS_EXTRA_EXCEPTION_INFO
#define ALT_CPU_HAS_ILLEGAL_INSTRUCTION_EXCEPTION
#define ALT_CPU_HAS_JMPI_INSTRUCTION
#define ALT_CPU_ICACHE_LINE_SIZE 32
#define ALT_CPU_ICACHE_LINE_SIZE_LOG2 5
#define ALT_CPU_ICACHE_SIZE 4096
#define ALT_CPU_INITDA_SUPPORTED
#define ALT_CPU_INST_ADDR_WIDTH 0x16
#define ALT_CPU_NAME "nios2_gen2_0"
#define ALT_CPU_NUM_OF_SHADOW_REG_SETS 0
#define ALT_CPU_OCI_VERSION 1
#define ALT_CPU_RESET_ADDR 0x00100000


/*
 * CPU configuration (with legacy prefix - don't use these anymore)
 *
 */

#define NIOS2_BIG_ENDIAN 0
#define NIOS2_BREAK_ADDR 0x00200820
#define NIOS2_CPU_ARCH_NIOS2_R1
#define NIOS2_CPU_FREQ 50000000u
#define NIOS2_CPU_ID_SIZE 1
#define NIOS2_CPU_ID_VALUE 0x00000000
#define NIOS2_CPU_IMPLEMENTATION "fast"
#define NIOS2_DATA_ADDR_WIDTH 0x16
#define NIOS2_DCACHE_BYPASS_MASK 0x80000000
#define NIOS2_DCACHE_LINE_SIZE 32
#define NIOS2_DCACHE_LINE_SIZE_LOG2 5
#define NIOS2_DCACHE_SIZE 2048
#define NIOS2_EXCEPTION_ADDR 0x00100020
#define NIOS2_FLASH_ACCELERATOR_LINES 0
#define NIOS2_FLASH_ACCELERATOR_LINE_SIZE 0
#define NIOS2_FLUSHDA_SUPPORTED
#define NIOS2_HARDWARE_DIVIDE_PRESENT 0
#define NIOS2_HARDWARE_MULTIPLY_PRESENT 1
#define NIOS2_HARDWARE_MULX_PRESENT 0
#define NIOS2_HAS_DEBUG_CORE 1
#define NIOS2_HAS_DEBUG_STUB
#define NIOS2_HAS_EXTRA_EXCEPTION_INFO
#define NIOS2_HAS_ILLEGAL_INSTRUCTION_EXCEPTION
#define NIOS2_HAS_JMPI_INSTRUCTION
#define NIOS2_ICACHE_LINE_SIZE 32
#define NIOS2_ICACHE_LINE_SIZE_LOG2 5
#define NIOS2_ICACHE_SIZE 4096
#define NIOS2_INITDA_SUPPORTED
#define NIOS2_INST_ADDR_WIDTH 0x16
#define NIOS2_NUM_OF_SHADOW_REG_SETS 0
#define NIOS2_OCI_VERSION 1
#define NIOS2_RESET_ADDR 0x00100000


/*
 * Define for each module class mastered by the CPU
 *
 */

#define __ALTERA_AVALON_JTAG_UART
#define __ALTERA_AVALON_PIO
#define __ALTERA_AVALON_SYSID_QSYS
#define __ALTERA_AVALON_TIMER
#define __ALTERA_AVALON_UART
#define __ALTERA_NIOS2_GEN2
#define __TERASIC_SRAM


/*
 * System configuration
 *
 */

#define ALT_DEVICE_FAMILY "Cyclone V"
#define ALT_ENHANCED_INTERRUPT_API_PRESENT
#define ALT_IRQ_BASE NULL
#define ALT_LOG_PORT "/dev/null"
#define ALT_LOG_PORT_BASE 0x0
#define ALT_LOG_PORT_DEV null
#define ALT_LOG_PORT_TYPE ""
#define ALT_NUM_EXTERNAL_INTERRUPT_CONTROLLERS 0
#define ALT_NUM_INTERNAL_INTERRUPT_CONTROLLERS 1
#define ALT_NUM_INTERRUPT_CONTROLLERS 1
#define ALT_STDERR "/dev/jtag_uart_0"
#define ALT_STDERR_BASE 0x201358
#define ALT_STDERR_DEV jtag_uart_0
#define ALT_STDERR_IS_JTAG_UART
#define ALT_STDERR_PRESENT
#define ALT_STDERR_TYPE "altera_avalon_jtag_uart"
#define ALT_STDIN "/dev/jtag_uart_0"
#define ALT_STDIN_BASE 0x201358
#define ALT_STDIN_DEV jtag_uart_0
#define ALT_STDIN_IS_JTAG_UART
#define ALT_STDIN_PRESENT
#define ALT_STDIN_TYPE "altera_avalon_jtag_uart"
#define ALT_STDOUT "/dev/jtag_uart_0"
#define ALT_STDOUT_BASE 0x201358
#define ALT_STDOUT_DEV jtag_uart_0
#define ALT_STDOUT_IS_JTAG_UART
#define ALT_STDOUT_PRESENT
#define ALT_STDOUT_TYPE "altera_avalon_jtag_uart"
#define ALT_SYSTEM_NAME "diniosaur"


/*
 * TERASIC_SRAM_0 configuration
 *
 */

#define ALT_MODULE_CLASS_TERASIC_SRAM_0 TERASIC_SRAM
#define TERASIC_SRAM_0_BASE 0x100000
#define TERASIC_SRAM_0_IRQ -1
#define TERASIC_SRAM_0_IRQ_INTERRUPT_CONTROLLER_ID -1
#define TERASIC_SRAM_0_NAME "/dev/TERASIC_SRAM_0"
#define TERASIC_SRAM_0_SPAN 1048576
#define TERASIC_SRAM_0_TYPE "TERASIC_SRAM"


/*
 * cactus_0 configuration
 *
 */

#define ALT_MODULE_CLASS_cactus_0 altera_avalon_pio
#define CACTUS_0_BASE 0x201290
#define CACTUS_0_BIT_CLEARING_EDGE_REGISTER 0
#define CACTUS_0_BIT_MODIFYING_OUTPUT_REGISTER 0
#define CACTUS_0_CAPTURE 0
#define CACTUS_0_DATA_WIDTH 11
#define CACTUS_0_DO_TEST_BENCH_WIRING 0
#define CACTUS_0_DRIVEN_SIM_VALUE 0
#define CACTUS_0_EDGE_TYPE "NONE"
#define CACTUS_0_FREQ 50000000
#define CACTUS_0_HAS_IN 0
#define CACTUS_0_HAS_OUT 1
#define CACTUS_0_HAS_TRI 0
#define CACTUS_0_IRQ -1
#define CACTUS_0_IRQ_INTERRUPT_CONTROLLER_ID -1
#define CACTUS_0_IRQ_TYPE "NONE"
#define CACTUS_0_NAME "/dev/cactus_0"
#define CACTUS_0_RESET_VALUE 0
#define CACTUS_0_SPAN 16
#define CACTUS_0_TYPE "altera_avalon_pio"


/*
 * cactus_1 configuration
 *
 */

#define ALT_MODULE_CLASS_cactus_1 altera_avalon_pio
#define CACTUS_1_BASE 0x201280
#define CACTUS_1_BIT_CLEARING_EDGE_REGISTER 0
#define CACTUS_1_BIT_MODIFYING_OUTPUT_REGISTER 0
#define CACTUS_1_CAPTURE 0
#define CACTUS_1_DATA_WIDTH 11
#define CACTUS_1_DO_TEST_BENCH_WIRING 0
#define CACTUS_1_DRIVEN_SIM_VALUE 0
#define CACTUS_1_EDGE_TYPE "NONE"
#define CACTUS_1_FREQ 50000000
#define CACTUS_1_HAS_IN 0
#define CACTUS_1_HAS_OUT 1
#define CACTUS_1_HAS_TRI 0
#define CACTUS_1_IRQ -1
#define CACTUS_1_IRQ_INTERRUPT_CONTROLLER_ID -1
#define CACTUS_1_IRQ_TYPE "NONE"
#define CACTUS_1_NAME "/dev/cactus_1"
#define CACTUS_1_RESET_VALUE 0
#define CACTUS_1_SPAN 16
#define CACTUS_1_TYPE "altera_avalon_pio"


/*
 * cactus_2 configuration
 *
 */

#define ALT_MODULE_CLASS_cactus_2 altera_avalon_pio
#define CACTUS_2_BASE 0x201270
#define CACTUS_2_BIT_CLEARING_EDGE_REGISTER 0
#define CACTUS_2_BIT_MODIFYING_OUTPUT_REGISTER 0
#define CACTUS_2_CAPTURE 0
#define CACTUS_2_DATA_WIDTH 11
#define CACTUS_2_DO_TEST_BENCH_WIRING 0
#define CACTUS_2_DRIVEN_SIM_VALUE 0
#define CACTUS_2_EDGE_TYPE "NONE"
#define CACTUS_2_FREQ 50000000
#define CACTUS_2_HAS_IN 0
#define CACTUS_2_HAS_OUT 1
#define CACTUS_2_HAS_TRI 0
#define CACTUS_2_IRQ -1
#define CACTUS_2_IRQ_INTERRUPT_CONTROLLER_ID -1
#define CACTUS_2_IRQ_TYPE "NONE"
#define CACTUS_2_NAME "/dev/cactus_2"
#define CACTUS_2_RESET_VALUE 0
#define CACTUS_2_SPAN 16
#define CACTUS_2_TYPE "altera_avalon_pio"


/*
 * cactus_icon_dim configuration
 *
 */

#define ALT_MODULE_CLASS_cactus_icon_dim altera_avalon_pio
#define CACTUS_ICON_DIM_BASE 0x2012e0
#define CACTUS_ICON_DIM_BIT_CLEARING_EDGE_REGISTER 0
#define CACTUS_ICON_DIM_BIT_MODIFYING_OUTPUT_REGISTER 0
#define CACTUS_ICON_DIM_CAPTURE 0
#define CACTUS_ICON_DIM_DATA_WIDTH 20
#define CACTUS_ICON_DIM_DO_TEST_BENCH_WIRING 0
#define CACTUS_ICON_DIM_DRIVEN_SIM_VALUE 0
#define CACTUS_ICON_DIM_EDGE_TYPE "NONE"
#define CACTUS_ICON_DIM_FREQ 50000000
#define CACTUS_ICON_DIM_HAS_IN 1
#define CACTUS_ICON_DIM_HAS_OUT 0
#define CACTUS_ICON_DIM_HAS_TRI 0
#define CACTUS_ICON_DIM_IRQ -1
#define CACTUS_ICON_DIM_IRQ_INTERRUPT_CONTROLLER_ID -1
#define CACTUS_ICON_DIM_IRQ_TYPE "NONE"
#define CACTUS_ICON_DIM_NAME "/dev/cactus_icon_dim"
#define CACTUS_ICON_DIM_RESET_VALUE 0
#define CACTUS_ICON_DIM_SPAN 16
#define CACTUS_ICON_DIM_TYPE "altera_avalon_pio"


/*
 * cactus_y configuration
 *
 */

#define ALT_MODULE_CLASS_cactus_y altera_avalon_pio
#define CACTUS_Y_BASE 0x201210
#define CACTUS_Y_BIT_CLEARING_EDGE_REGISTER 0
#define CACTUS_Y_BIT_MODIFYING_OUTPUT_REGISTER 0
#define CACTUS_Y_CAPTURE 0
#define CACTUS_Y_DATA_WIDTH 10
#define CACTUS_Y_DO_TEST_BENCH_WIRING 0
#define CACTUS_Y_DRIVEN_SIM_VALUE 0
#define CACTUS_Y_EDGE_TYPE "NONE"
#define CACTUS_Y_FREQ 50000000
#define CACTUS_Y_HAS_IN 0
#define CACTUS_Y_HAS_OUT 1
#define CACTUS_Y_HAS_TRI 0
#define CACTUS_Y_IRQ -1
#define CACTUS_Y_IRQ_INTERRUPT_CONTROLLER_ID -1
#define CACTUS_Y_IRQ_TYPE "NONE"
#define CACTUS_Y_NAME "/dev/cactus_y"
#define CACTUS_Y_RESET_VALUE 0
#define CACTUS_Y_SPAN 16
#define CACTUS_Y_TYPE "altera_avalon_pio"


/*
 * cheat_mod configuration
 *
 */

#define ALT_MODULE_CLASS_cheat_mod altera_avalon_pio
#define CHEAT_MOD_BASE 0x2011f0
#define CHEAT_MOD_BIT_CLEARING_EDGE_REGISTER 0
#define CHEAT_MOD_BIT_MODIFYING_OUTPUT_REGISTER 0
#define CHEAT_MOD_CAPTURE 0
#define CHEAT_MOD_DATA_WIDTH 1
#define CHEAT_MOD_DO_TEST_BENCH_WIRING 0
#define CHEAT_MOD_DRIVEN_SIM_VALUE 0
#define CHEAT_MOD_EDGE_TYPE "NONE"
#define CHEAT_MOD_FREQ 50000000
#define CHEAT_MOD_HAS_IN 0
#define CHEAT_MOD_HAS_OUT 1
#define CHEAT_MOD_HAS_TRI 0
#define CHEAT_MOD_IRQ -1
#define CHEAT_MOD_IRQ_INTERRUPT_CONTROLLER_ID -1
#define CHEAT_MOD_IRQ_TYPE "NONE"
#define CHEAT_MOD_NAME "/dev/cheat_mod"
#define CHEAT_MOD_RESET_VALUE 0
#define CHEAT_MOD_SPAN 16
#define CHEAT_MOD_TYPE "altera_avalon_pio"


/*
 * cloud_0 configuration
 *
 */

#define ALT_MODULE_CLASS_cloud_0 altera_avalon_pio
#define CLOUD_0_BASE 0x2012c0
#define CLOUD_0_BIT_CLEARING_EDGE_REGISTER 0
#define CLOUD_0_BIT_MODIFYING_OUTPUT_REGISTER 0
#define CLOUD_0_CAPTURE 0
#define CLOUD_0_DATA_WIDTH 21
#define CLOUD_0_DO_TEST_BENCH_WIRING 0
#define CLOUD_0_DRIVEN_SIM_VALUE 0
#define CLOUD_0_EDGE_TYPE "NONE"
#define CLOUD_0_FREQ 50000000
#define CLOUD_0_HAS_IN 0
#define CLOUD_0_HAS_OUT 1
#define CLOUD_0_HAS_TRI 0
#define CLOUD_0_IRQ -1
#define CLOUD_0_IRQ_INTERRUPT_CONTROLLER_ID -1
#define CLOUD_0_IRQ_TYPE "NONE"
#define CLOUD_0_NAME "/dev/cloud_0"
#define CLOUD_0_RESET_VALUE 0
#define CLOUD_0_SPAN 16
#define CLOUD_0_TYPE "altera_avalon_pio"


/*
 * cloud_1 configuration
 *
 */

#define ALT_MODULE_CLASS_cloud_1 altera_avalon_pio
#define CLOUD_1_BASE 0x2012b0
#define CLOUD_1_BIT_CLEARING_EDGE_REGISTER 0
#define CLOUD_1_BIT_MODIFYING_OUTPUT_REGISTER 0
#define CLOUD_1_CAPTURE 0
#define CLOUD_1_DATA_WIDTH 21
#define CLOUD_1_DO_TEST_BENCH_WIRING 0
#define CLOUD_1_DRIVEN_SIM_VALUE 0
#define CLOUD_1_EDGE_TYPE "NONE"
#define CLOUD_1_FREQ 50000000
#define CLOUD_1_HAS_IN 0
#define CLOUD_1_HAS_OUT 1
#define CLOUD_1_HAS_TRI 0
#define CLOUD_1_IRQ -1
#define CLOUD_1_IRQ_INTERRUPT_CONTROLLER_ID -1
#define CLOUD_1_IRQ_TYPE "NONE"
#define CLOUD_1_NAME "/dev/cloud_1"
#define CLOUD_1_RESET_VALUE 0
#define CLOUD_1_SPAN 16
#define CLOUD_1_TYPE "altera_avalon_pio"


/*
 * cloud_2 configuration
 *
 */

#define ALT_MODULE_CLASS_cloud_2 altera_avalon_pio
#define CLOUD_2_BASE 0x2012a0
#define CLOUD_2_BIT_CLEARING_EDGE_REGISTER 0
#define CLOUD_2_BIT_MODIFYING_OUTPUT_REGISTER 0
#define CLOUD_2_CAPTURE 0
#define CLOUD_2_DATA_WIDTH 21
#define CLOUD_2_DO_TEST_BENCH_WIRING 0
#define CLOUD_2_DRIVEN_SIM_VALUE 0
#define CLOUD_2_EDGE_TYPE "NONE"
#define CLOUD_2_FREQ 50000000
#define CLOUD_2_HAS_IN 0
#define CLOUD_2_HAS_OUT 1
#define CLOUD_2_HAS_TRI 0
#define CLOUD_2_IRQ -1
#define CLOUD_2_IRQ_INTERRUPT_CONTROLLER_ID -1
#define CLOUD_2_IRQ_TYPE "NONE"
#define CLOUD_2_NAME "/dev/cloud_2"
#define CLOUD_2_RESET_VALUE 0
#define CLOUD_2_SPAN 16
#define CLOUD_2_TYPE "altera_avalon_pio"


/*
 * cloud_icon_dim configuration
 *
 */

#define ALT_MODULE_CLASS_cloud_icon_dim altera_avalon_pio
#define CLOUD_ICON_DIM_BASE 0x2012d0
#define CLOUD_ICON_DIM_BIT_CLEARING_EDGE_REGISTER 0
#define CLOUD_ICON_DIM_BIT_MODIFYING_OUTPUT_REGISTER 0
#define CLOUD_ICON_DIM_CAPTURE 0
#define CLOUD_ICON_DIM_DATA_WIDTH 20
#define CLOUD_ICON_DIM_DO_TEST_BENCH_WIRING 0
#define CLOUD_ICON_DIM_DRIVEN_SIM_VALUE 0
#define CLOUD_ICON_DIM_EDGE_TYPE "NONE"
#define CLOUD_ICON_DIM_FREQ 50000000
#define CLOUD_ICON_DIM_HAS_IN 1
#define CLOUD_ICON_DIM_HAS_OUT 0
#define CLOUD_ICON_DIM_HAS_TRI 0
#define CLOUD_ICON_DIM_IRQ -1
#define CLOUD_ICON_DIM_IRQ_INTERRUPT_CONTROLLER_ID -1
#define CLOUD_ICON_DIM_IRQ_TYPE "NONE"
#define CLOUD_ICON_DIM_NAME "/dev/cloud_icon_dim"
#define CLOUD_ICON_DIM_RESET_VALUE 0
#define CLOUD_ICON_DIM_SPAN 16
#define CLOUD_ICON_DIM_TYPE "altera_avalon_pio"


/*
 * collision configuration
 *
 */

#define ALT_MODULE_CLASS_collision altera_avalon_pio
#define COLLISION_BASE 0x201310
#define COLLISION_BIT_CLEARING_EDGE_REGISTER 0
#define COLLISION_BIT_MODIFYING_OUTPUT_REGISTER 0
#define COLLISION_CAPTURE 1
#define COLLISION_DATA_WIDTH 1
#define COLLISION_DO_TEST_BENCH_WIRING 0
#define COLLISION_DRIVEN_SIM_VALUE 0
#define COLLISION_EDGE_TYPE "RISING"
#define COLLISION_FREQ 50000000
#define COLLISION_HAS_IN 1
#define COLLISION_HAS_OUT 0
#define COLLISION_HAS_TRI 0
#define COLLISION_IRQ 4
#define COLLISION_IRQ_INTERRUPT_CONTROLLER_ID 0
#define COLLISION_IRQ_TYPE "EDGE"
#define COLLISION_NAME "/dev/collision"
#define COLLISION_RESET_VALUE 0
#define COLLISION_SPAN 16
#define COLLISION_TYPE "altera_avalon_pio"


/*
 * dino_icon_dim configuration
 *
 */

#define ALT_MODULE_CLASS_dino_icon_dim altera_avalon_pio
#define DINO_ICON_DIM_BASE 0x2012f0
#define DINO_ICON_DIM_BIT_CLEARING_EDGE_REGISTER 0
#define DINO_ICON_DIM_BIT_MODIFYING_OUTPUT_REGISTER 0
#define DINO_ICON_DIM_CAPTURE 0
#define DINO_ICON_DIM_DATA_WIDTH 20
#define DINO_ICON_DIM_DO_TEST_BENCH_WIRING 0
#define DINO_ICON_DIM_DRIVEN_SIM_VALUE 0
#define DINO_ICON_DIM_EDGE_TYPE "NONE"
#define DINO_ICON_DIM_FREQ 50000000
#define DINO_ICON_DIM_HAS_IN 1
#define DINO_ICON_DIM_HAS_OUT 0
#define DINO_ICON_DIM_HAS_TRI 0
#define DINO_ICON_DIM_IRQ -1
#define DINO_ICON_DIM_IRQ_INTERRUPT_CONTROLLER_ID -1
#define DINO_ICON_DIM_IRQ_TYPE "NONE"
#define DINO_ICON_DIM_NAME "/dev/dino_icon_dim"
#define DINO_ICON_DIM_RESET_VALUE 0
#define DINO_ICON_DIM_SPAN 16
#define DINO_ICON_DIM_TYPE "altera_avalon_pio"


/*
 * dino_x configuration
 *
 */

#define ALT_MODULE_CLASS_dino_x altera_avalon_pio
#define DINO_X_BASE 0x201200
#define DINO_X_BIT_CLEARING_EDGE_REGISTER 0
#define DINO_X_BIT_MODIFYING_OUTPUT_REGISTER 0
#define DINO_X_CAPTURE 0
#define DINO_X_DATA_WIDTH 10
#define DINO_X_DO_TEST_BENCH_WIRING 0
#define DINO_X_DRIVEN_SIM_VALUE 0
#define DINO_X_EDGE_TYPE "NONE"
#define DINO_X_FREQ 50000000
#define DINO_X_HAS_IN 0
#define DINO_X_HAS_OUT 1
#define DINO_X_HAS_TRI 0
#define DINO_X_IRQ -1
#define DINO_X_IRQ_INTERRUPT_CONTROLLER_ID -1
#define DINO_X_IRQ_TYPE "NONE"
#define DINO_X_NAME "/dev/dino_x"
#define DINO_X_RESET_VALUE 0
#define DINO_X_SPAN 16
#define DINO_X_TYPE "altera_avalon_pio"


/*
 * dino_y configuration
 *
 */

#define ALT_MODULE_CLASS_dino_y altera_avalon_pio
#define DINO_Y_BASE 0x201330
#define DINO_Y_BIT_CLEARING_EDGE_REGISTER 0
#define DINO_Y_BIT_MODIFYING_OUTPUT_REGISTER 0
#define DINO_Y_CAPTURE 0
#define DINO_Y_DATA_WIDTH 10
#define DINO_Y_DO_TEST_BENCH_WIRING 0
#define DINO_Y_DRIVEN_SIM_VALUE 0
#define DINO_Y_EDGE_TYPE "NONE"
#define DINO_Y_FREQ 50000000
#define DINO_Y_HAS_IN 0
#define DINO_Y_HAS_OUT 1
#define DINO_Y_HAS_TRI 0
#define DINO_Y_IRQ -1
#define DINO_Y_IRQ_INTERRUPT_CONTROLLER_ID -1
#define DINO_Y_IRQ_TYPE "NONE"
#define DINO_Y_NAME "/dev/dino_y"
#define DINO_Y_RESET_VALUE 0
#define DINO_Y_SPAN 16
#define DINO_Y_TYPE "altera_avalon_pio"


/*
 * ground_color configuration
 *
 */

#define ALT_MODULE_CLASS_ground_color altera_avalon_pio
#define GROUND_COLOR_BASE 0x201220
#define GROUND_COLOR_BIT_CLEARING_EDGE_REGISTER 0
#define GROUND_COLOR_BIT_MODIFYING_OUTPUT_REGISTER 0
#define GROUND_COLOR_CAPTURE 0
#define GROUND_COLOR_DATA_WIDTH 24
#define GROUND_COLOR_DO_TEST_BENCH_WIRING 0
#define GROUND_COLOR_DRIVEN_SIM_VALUE 0
#define GROUND_COLOR_EDGE_TYPE "NONE"
#define GROUND_COLOR_FREQ 50000000
#define GROUND_COLOR_HAS_IN 0
#define GROUND_COLOR_HAS_OUT 1
#define GROUND_COLOR_HAS_TRI 0
#define GROUND_COLOR_IRQ -1
#define GROUND_COLOR_IRQ_INTERRUPT_CONTROLLER_ID -1
#define GROUND_COLOR_IRQ_TYPE "NONE"
#define GROUND_COLOR_NAME "/dev/ground_color"
#define GROUND_COLOR_RESET_VALUE 0
#define GROUND_COLOR_SPAN 16
#define GROUND_COLOR_TYPE "altera_avalon_pio"


/*
 * hal configuration
 *
 */

#define ALT_INCLUDE_INSTRUCTION_RELATED_EXCEPTION_API
#define ALT_MAX_FD 32
#define ALT_SYS_CLK TIMER_0
#define ALT_TIMESTAMP_CLK none


/*
 * horizon_level configuration
 *
 */

#define ALT_MODULE_CLASS_horizon_level altera_avalon_pio
#define HORIZON_LEVEL_BASE 0x201240
#define HORIZON_LEVEL_BIT_CLEARING_EDGE_REGISTER 0
#define HORIZON_LEVEL_BIT_MODIFYING_OUTPUT_REGISTER 0
#define HORIZON_LEVEL_CAPTURE 0
#define HORIZON_LEVEL_DATA_WIDTH 10
#define HORIZON_LEVEL_DO_TEST_BENCH_WIRING 0
#define HORIZON_LEVEL_DRIVEN_SIM_VALUE 0
#define HORIZON_LEVEL_EDGE_TYPE "NONE"
#define HORIZON_LEVEL_FREQ 50000000
#define HORIZON_LEVEL_HAS_IN 0
#define HORIZON_LEVEL_HAS_OUT 1
#define HORIZON_LEVEL_HAS_TRI 0
#define HORIZON_LEVEL_IRQ -1
#define HORIZON_LEVEL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define HORIZON_LEVEL_IRQ_TYPE "NONE"
#define HORIZON_LEVEL_NAME "/dev/horizon_level"
#define HORIZON_LEVEL_RESET_VALUE 0
#define HORIZON_LEVEL_SPAN 16
#define HORIZON_LEVEL_TYPE "altera_avalon_pio"


/*
 * image_sync configuration
 *
 */

#define ALT_MODULE_CLASS_image_sync altera_avalon_pio
#define IMAGE_SYNC_BASE 0x201320
#define IMAGE_SYNC_BIT_CLEARING_EDGE_REGISTER 0
#define IMAGE_SYNC_BIT_MODIFYING_OUTPUT_REGISTER 0
#define IMAGE_SYNC_CAPTURE 1
#define IMAGE_SYNC_DATA_WIDTH 1
#define IMAGE_SYNC_DO_TEST_BENCH_WIRING 0
#define IMAGE_SYNC_DRIVEN_SIM_VALUE 0
#define IMAGE_SYNC_EDGE_TYPE "RISING"
#define IMAGE_SYNC_FREQ 50000000
#define IMAGE_SYNC_HAS_IN 1
#define IMAGE_SYNC_HAS_OUT 0
#define IMAGE_SYNC_HAS_TRI 0
#define IMAGE_SYNC_IRQ 5
#define IMAGE_SYNC_IRQ_INTERRUPT_CONTROLLER_ID 0
#define IMAGE_SYNC_IRQ_TYPE "EDGE"
#define IMAGE_SYNC_NAME "/dev/image_sync"
#define IMAGE_SYNC_RESET_VALUE 0
#define IMAGE_SYNC_SPAN 16
#define IMAGE_SYNC_TYPE "altera_avalon_pio"


/*
 * jtag_uart_0 configuration
 *
 */

#define ALT_MODULE_CLASS_jtag_uart_0 altera_avalon_jtag_uart
#define JTAG_UART_0_BASE 0x201358
#define JTAG_UART_0_IRQ 2
#define JTAG_UART_0_IRQ_INTERRUPT_CONTROLLER_ID 0
#define JTAG_UART_0_NAME "/dev/jtag_uart_0"
#define JTAG_UART_0_READ_DEPTH 64
#define JTAG_UART_0_READ_THRESHOLD 8
#define JTAG_UART_0_SPAN 8
#define JTAG_UART_0_TYPE "altera_avalon_jtag_uart"
#define JTAG_UART_0_WRITE_DEPTH 64
#define JTAG_UART_0_WRITE_THRESHOLD 8


/*
 * keys configuration
 *
 */

#define ALT_MODULE_CLASS_keys altera_avalon_pio
#define KEYS_BASE 0x201300
#define KEYS_BIT_CLEARING_EDGE_REGISTER 1
#define KEYS_BIT_MODIFYING_OUTPUT_REGISTER 0
#define KEYS_CAPTURE 1
#define KEYS_DATA_WIDTH 4
#define KEYS_DO_TEST_BENCH_WIRING 0
#define KEYS_DRIVEN_SIM_VALUE 0
#define KEYS_EDGE_TYPE "FALLING"
#define KEYS_FREQ 50000000
#define KEYS_HAS_IN 1
#define KEYS_HAS_OUT 0
#define KEYS_HAS_TRI 0
#define KEYS_IRQ 3
#define KEYS_IRQ_INTERRUPT_CONTROLLER_ID 0
#define KEYS_IRQ_TYPE "EDGE"
#define KEYS_NAME "/dev/keys"
#define KEYS_RESET_VALUE 0
#define KEYS_SPAN 16
#define KEYS_TYPE "altera_avalon_pio"


/*
 * life configuration
 *
 */

#define ALT_MODULE_CLASS_life altera_avalon_pio
#define LIFE_BASE 0x2011e0
#define LIFE_BIT_CLEARING_EDGE_REGISTER 0
#define LIFE_BIT_MODIFYING_OUTPUT_REGISTER 0
#define LIFE_CAPTURE 0
#define LIFE_DATA_WIDTH 9
#define LIFE_DO_TEST_BENCH_WIRING 0
#define LIFE_DRIVEN_SIM_VALUE 0
#define LIFE_EDGE_TYPE "NONE"
#define LIFE_FREQ 50000000
#define LIFE_HAS_IN 0
#define LIFE_HAS_OUT 1
#define LIFE_HAS_TRI 0
#define LIFE_IRQ -1
#define LIFE_IRQ_INTERRUPT_CONTROLLER_ID -1
#define LIFE_IRQ_TYPE "NONE"
#define LIFE_NAME "/dev/life"
#define LIFE_RESET_VALUE 0
#define LIFE_SPAN 16
#define LIFE_TYPE "altera_avalon_pio"


/*
 * score configuration
 *
 */

#define ALT_MODULE_CLASS_score altera_avalon_pio
#define SCORE_BASE 0x201260
#define SCORE_BIT_CLEARING_EDGE_REGISTER 0
#define SCORE_BIT_MODIFYING_OUTPUT_REGISTER 0
#define SCORE_CAPTURE 0
#define SCORE_DATA_WIDTH 16
#define SCORE_DO_TEST_BENCH_WIRING 0
#define SCORE_DRIVEN_SIM_VALUE 0
#define SCORE_EDGE_TYPE "NONE"
#define SCORE_FREQ 50000000
#define SCORE_HAS_IN 0
#define SCORE_HAS_OUT 1
#define SCORE_HAS_TRI 0
#define SCORE_IRQ -1
#define SCORE_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SCORE_IRQ_TYPE "NONE"
#define SCORE_NAME "/dev/score"
#define SCORE_RESET_VALUE 0
#define SCORE_SPAN 16
#define SCORE_TYPE "altera_avalon_pio"


/*
 * sky_color configuration
 *
 */

#define ALT_MODULE_CLASS_sky_color altera_avalon_pio
#define SKY_COLOR_BASE 0x201230
#define SKY_COLOR_BIT_CLEARING_EDGE_REGISTER 0
#define SKY_COLOR_BIT_MODIFYING_OUTPUT_REGISTER 0
#define SKY_COLOR_CAPTURE 0
#define SKY_COLOR_DATA_WIDTH 24
#define SKY_COLOR_DO_TEST_BENCH_WIRING 0
#define SKY_COLOR_DRIVEN_SIM_VALUE 0
#define SKY_COLOR_EDGE_TYPE "NONE"
#define SKY_COLOR_FREQ 50000000
#define SKY_COLOR_HAS_IN 0
#define SKY_COLOR_HAS_OUT 1
#define SKY_COLOR_HAS_TRI 0
#define SKY_COLOR_IRQ -1
#define SKY_COLOR_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SKY_COLOR_IRQ_TYPE "NONE"
#define SKY_COLOR_NAME "/dev/sky_color"
#define SKY_COLOR_RESET_VALUE 0
#define SKY_COLOR_SPAN 16
#define SKY_COLOR_TYPE "altera_avalon_pio"


/*
 * state configuration
 *
 */

#define ALT_MODULE_CLASS_state altera_avalon_pio
#define STATE_BASE 0x201250
#define STATE_BIT_CLEARING_EDGE_REGISTER 0
#define STATE_BIT_MODIFYING_OUTPUT_REGISTER 0
#define STATE_CAPTURE 0
#define STATE_DATA_WIDTH 2
#define STATE_DO_TEST_BENCH_WIRING 0
#define STATE_DRIVEN_SIM_VALUE 0
#define STATE_EDGE_TYPE "NONE"
#define STATE_FREQ 50000000
#define STATE_HAS_IN 0
#define STATE_HAS_OUT 1
#define STATE_HAS_TRI 0
#define STATE_IRQ -1
#define STATE_IRQ_INTERRUPT_CONTROLLER_ID -1
#define STATE_IRQ_TYPE "NONE"
#define STATE_NAME "/dev/state"
#define STATE_RESET_VALUE 0
#define STATE_SPAN 16
#define STATE_TYPE "altera_avalon_pio"


/*
 * sysid_qsys_0 configuration
 *
 */

#define ALT_MODULE_CLASS_sysid_qsys_0 altera_avalon_sysid_qsys
#define SYSID_QSYS_0_BASE 0x201350
#define SYSID_QSYS_0_ID 0
#define SYSID_QSYS_0_IRQ -1
#define SYSID_QSYS_0_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SYSID_QSYS_0_NAME "/dev/sysid_qsys_0"
#define SYSID_QSYS_0_SPAN 8
#define SYSID_QSYS_0_TIMESTAMP 1574789022
#define SYSID_QSYS_0_TYPE "altera_avalon_sysid_qsys"


/*
 * timer_0 configuration
 *
 */

#define ALT_MODULE_CLASS_timer_0 altera_avalon_timer
#define TIMER_0_ALWAYS_RUN 0
#define TIMER_0_BASE 0x201060
#define TIMER_0_COUNTER_SIZE 32
#define TIMER_0_FIXED_PERIOD 0
#define TIMER_0_FREQ 50000000
#define TIMER_0_IRQ 1
#define TIMER_0_IRQ_INTERRUPT_CONTROLLER_ID 0
#define TIMER_0_LOAD_VALUE 49999
#define TIMER_0_MULT 0.001
#define TIMER_0_NAME "/dev/timer_0"
#define TIMER_0_PERIOD 1
#define TIMER_0_PERIOD_UNITS "ms"
#define TIMER_0_RESET_OUTPUT 0
#define TIMER_0_SNAPSHOT 1
#define TIMER_0_SPAN 32
#define TIMER_0_TICKS_PER_SEC 1000
#define TIMER_0_TIMEOUT_PULSE_OUTPUT 0
#define TIMER_0_TYPE "altera_avalon_timer"


/*
 * uart_0 configuration
 *
 */

#define ALT_MODULE_CLASS_uart_0 altera_avalon_uart
#define UART_0_BASE 0x201040
#define UART_0_BAUD 115200
#define UART_0_DATA_BITS 8
#define UART_0_FIXED_BAUD 1
#define UART_0_FREQ 50000000
#define UART_0_IRQ 0
#define UART_0_IRQ_INTERRUPT_CONTROLLER_ID 0
#define UART_0_NAME "/dev/uart_0"
#define UART_0_PARITY 'N'
#define UART_0_SIM_CHAR_STREAM ""
#define UART_0_SIM_TRUE_BAUD 0
#define UART_0_SPAN 32
#define UART_0_STOP_BITS 1
#define UART_0_SYNC_REG_DEPTH 2
#define UART_0_TYPE "altera_avalon_uart"
#define UART_0_USE_CTS_RTS 0
#define UART_0_USE_EOP_REGISTER 0

#endif /* __SYSTEM_H_ */
