/*
 * configuration.h
 *
 *  Created on: 30 oct. 2019
 *      Author: legalllasall_xav
 */

#include "system.h"

#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_

#define GAME_OVER_MAX_DURATION_MS 5000 // After this duration, the game_over state automatically turns to idle state if nothing happens
#define CACTUS_TO_CLOUD_SPEED_RATIO 5 // Clouds are shifted slower than cacti because they are further

#define MAX_GAME_SPEED 320 // px/s
#define MIN_GAME_SPEED 100 // px/s
#define GAME_SPEED_RAMP_DURATION_S 120 // s

#define INVINCIBILITY_DURATION_AFTER_COLLIDING 2 // s

#define LIFE_LEVEL_INIT 5 // when a new session starts

#define FLOOR_LEVEL 50 // Bottom of cacti and dinosaur when not jumping
#define HORIZON_LEVEL 80 // Sky limit

#define AVERAGE_CLOUD_LEVEL 100

#define DINO_JUMP_HEIGHT 220 // px
#define DINO_JUMP_DURATION_MS 1400 // ms
#define DINO_POSX 20 // dino x location, only the y coordinate changes (px)

#define MIN_DIST_BTW_CACTI 150 // minimum distance between 2 consecutive cacti (px)

#define MIN_DIST_BTW_CLOUDS 80 // minimum distance between 2 consecutive clouds (px)

#define WINDOW_HEIGHT 480 // px
#define WINDOW_WIDTH 640 // px

// colors (must be between 0 and 255)

#define SKY_COLOR_R 66
#define SKY_COLOR_G 176
#define SKY_COLOR_B 245

#define GROUND_COLOR_R 245
#define GROUND_COLOR_G 218
#define GROUND_COLOR_B 66

#define TPERIOD ((TIMER_0_FREQ*TIMER_0_MULT)*1)
#define TPERIOD_MS ((int)TPERIOD & 0x0000FFFF)*2/100000 //TODO: understand

#endif /* CONFIGURATION_H_ */
