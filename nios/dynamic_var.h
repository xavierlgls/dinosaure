/*
 * dynamic_var.h
 *
 *  Created on: 30 oct. 2019
 *      Author: legalllasall_xav
 */

#ifndef DYNAMIC_VAR_H_
#define DYNAMIC_VAR_H_

state currentState;

extern long timeMs;
extern int gameFinishDateMs;
extern long dinoJumpBeginDateMs;
extern int ignoringCollisionUntilMs;
extern int life;

extern bool dinoJumping;
extern bool waitingForReset;
extern bool ignoringCollision;

extern bool key_IRQ ;
extern bool sync_IRQ;
extern bool collision_IRQ;
extern bool timer_IRQ;
extern volatile int edge_capture;

extern int cloudIcon_height;
extern int cloudIcon_width;
extern int dinoIcon_height;
extern int dinoIcon_width;
extern int cactusIcon_height;
extern int cactusIcon_width;

extern cloud clouds[3];
extern cactus cacti[3];

#endif /* DYNAMIC_VAR_H_ */
