/*
 * dynamic_var.c
 */

#include "my_types.h"
#include "configuration.h"

#ifndef DYN_VAR
#define DYN_VAR

// All dynamic variables are declared there

state currentState = IDLE; // Current state

long timeMs = 0; // Time elapsed since the beginning of the last game (ms)
long gameFinishDateMs = 0; // Duration of the last finished game (ms)
long dinoJumpBeginDateMs = 0; // Date of the beginning of the jump (ms)
long ignoringCollisionUntilMs = 0; // Date when the collision is not ignored anymore
int life = LIFE_LEVEL_INIT;

bool dinoJumping = FALSE; // If true, the jump animation is being executed
bool waitingForReset = FALSE; // If game_over state back to idle (reset) after a fixed duration
bool ignoringCollision = FALSE;

// IRQ flags
bool key_IRQ = FALSE;
bool sync_IRQ = FALSE;
bool collision_IRQ = FALSE;
bool timer_IRQ = FALSE;
/* To hold the value of the key edge_capture register. */
volatile int edge_capture;

int cloudIcon_height;
int cloudIcon_width;

int dinoIcon_height;
int dinoIcon_width;

int cactusIcon_height;
int cactusIcon_width;

cloud clouds[]={
		{
				.visible = FALSE,
				.posX = 0,
				.posY = 0,
				.creationTimeMs = 0
		},
		{
				.visible = FALSE,
				.posX = 0,
				.posY = 0,
				.creationTimeMs = 0
		},
		{
				.visible = FALSE,
				.posX = 0,
				.posY = 0,
				.creationTimeMs = 0
		}
};

cactus cacti[]={
		{
				.visible = FALSE,
				.posX = 0,
				.creationTimeMs = 0
		},
		{
				.visible = FALSE,
				.posX = 0,
				.creationTimeMs = 0
		},
		{
				.visible = FALSE,
				.posX = 0,
				.creationTimeMs = 0
		}
};

#endif /*DYN_VAR*/
