#ifndef STATES_MACHINE_H_
#define STATES_MACHINE_H_

void transitionIdleToPlaying();
void transitionPlayingToPause();
void transitionPauseToPlaying();
void transitionPlayingToGameOver();
void transitionGameOverToPlaying();
void transitionGameOverToIdle();
void setState(enum State nextState);

#endif /*STATES_MACHINE_H_*/