/*
 * interruptions.h
 *
 *  Created on: 12 nov. 2019
 *      Author: legalllasall_xav
 */

#ifndef INTERRUPTIONS_H_
#define INTERRUPTIONS_H_

void enableCheatMod();
void disableCheatMod();

int enableCollisionInterrupt(bool enabled);
int initInterruptions();
void onTimerInterrupt(void* context);
void onKeyInterrupt(void* context);
void onCollisionInterrupt(void* context);
void onSyncInterrupt(void* context);

#endif /* INTERRUPTIONS_H_ */
