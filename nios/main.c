#include "my_includes.h"

bool initVar(){
	// init random generator
	srand(time(NULL));

	// get values from VHDL constants
	cloudIcon_height = IORD_ALTERA_AVALON_PIO_DATA(CLOUD_ICON_DIM_BASE) >> 10; // get only first 10 bits
	cloudIcon_width = IORD_ALTERA_AVALON_PIO_DATA(CLOUD_ICON_DIM_BASE) & 1023; // get only last 10 bits
	dinoIcon_height = IORD_ALTERA_AVALON_PIO_DATA(DINO_ICON_DIM_BASE) >> 10; // get only first 10 bits
	dinoIcon_width = IORD_ALTERA_AVALON_PIO_DATA(DINO_ICON_DIM_BASE) & 1023; // get only last 10 bits
	cactusIcon_height = IORD_ALTERA_AVALON_PIO_DATA(CACTUS_ICON_DIM_BASE) >> 10; // get only first 10 bits
	cactusIcon_width = IORD_ALTERA_AVALON_PIO_DATA(CACTUS_ICON_DIM_BASE) & 1023; // get only last 10 bits

	// send config values to the vhdl
	// Static values defined config file
	IOWR_ALTERA_AVALON_PIO_DATA(HORIZON_LEVEL_BASE, (WINDOW_HEIGHT - HORIZON_LEVEL));
	IOWR_ALTERA_AVALON_PIO_DATA(SKY_COLOR_BASE, (SKY_COLOR_R * pow(2,16) + SKY_COLOR_G * pow(2,8) + SKY_COLOR_B));
	IOWR_ALTERA_AVALON_PIO_DATA(GROUND_COLOR_BASE, (GROUND_COLOR_R * pow(2,16) + GROUND_COLOR_G * pow(2,8) + GROUND_COLOR_B));
	IOWR_ALTERA_AVALON_PIO_DATA(DINO_X_BASE, DINO_POSX);
	IOWR_ALTERA_AVALON_PIO_DATA(CACTUS_Y_BASE, WINDOW_HEIGHT - FLOOR_LEVEL - cactusIcon_height);
	IOWR_ALTERA_AVALON_PIO_DATA(LIFE_BASE, LIFE_LEVEL_INIT);

	return TRUE;
}

void starTimer(){
	IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_0_BASE, (IORD_ALTERA_AVALON_TIMER_CONTROL(TIMER_0_BASE) | ALTERA_AVALON_TIMER_CONTROL_START_MSK));
}

void handleTimerEvent(){
	if (currentState == PLAYING || currentState == GAME_OVER){
		timeMs += TPERIOD_MS;
		if(timeMs >= ignoringCollisionUntilMs && ignoringCollisionUntilMs != 0){
			ignoringCollisionUntilMs = 0;
			disableCheatMod();
		}
		if (currentState == PLAYING){
			displayScore(timeMs/100);
		}
	}
	if(waitingForReset && (timeMs - gameFinishDateMs) > GAME_OVER_MAX_DURATION_MS){
		transitionGameOverToIdle();
	}
}

void handleCollision(){
	if (currentState == PLAYING && !ignoringCollision){
		life--;
		displayLevel(life);
		printf("life: %d\n", life);
		if(life <= 0){
			transitionPlayingToGameOver();
		}
		ignoringCollisionUntilMs = timeMs + INVINCIBILITY_DURATION_AFTER_COLLIDING * 1000;
		enableCheatMod();
	}
}

void handleKey0(){
	printf("Key0 event\n");
	if (currentState == IDLE){
		transitionIdleToPlaying();
	} else if (currentState == GAME_OVER){
		transitionGameOverToPlaying();
	} else if (currentState == PLAYING){
		jumpDino();
	}
}

void handleKey1(){
	printf("Key1 event\n");
	if (currentState == PLAYING){
		transitionPlayingToPause();
	} else if (currentState == PAUSE){
		transitionPauseToPlaying();
	}
}

void handleKey2(){
	printf("Key2 event\n");
	//nothing yet
}

void handleKey3(){
	printf("Key3 event\n");
	if (ignoringCollision){
		disableCheatMod();
		printf("cheat mode disabled\n");
	}else{
		enableCheatMod();
		printf("cheat mode enabled\n");
	}
}

void handleSync(){
	//	printf("Sync event\n");
	if (currentState == PLAYING){
		displayDino();
		displayCacti();
		displayClouds();
	} else if (currentState == IDLE){
		displayDinoCentered();
		hideCacti();
		hideClouds();
	} else if (currentState == GAME_OVER) {
		displayDinoCentered();
		hideCacti();
		hideClouds();
	}
}

bool init(void){
	bool bSuccess = TRUE;

	if (!initInterruptions()){
		printf("[ERROR] registering ISR\n");
		bSuccess = FALSE;
	}

	if(!initVar()){
		printf("[ERROR] variable initialization\n");
		bSuccess = FALSE;
	}

	return bSuccess;
}

int main()
{
	if (!init()){
		return 0;
	}
	else{
		starTimer();
	}
	printf("nios initialized\n");
	while (1)
	{
		if(timer_IRQ){
			timer_IRQ = FALSE;
			handleTimerEvent();
		}
		if(collision_IRQ){
			collision_IRQ = FALSE;
			handleCollision();
		}
		if(sync_IRQ){
			sync_IRQ = FALSE;
			handleSync();
		}
		if (key_IRQ){
			key_IRQ = FALSE;
			switch(edge_capture){
			case 0x1 : // KEY0
				handleKey0();
				break;
			case 0x2 : // KEY1
				handleKey1();
				break;
			case 0x4 : // KEY2
				handleKey2();
				break;
			case 0x8 : // KEY3
				handleKey3();
				break;
			default :
				printf(" ... unsupported KEY\n");
				break;
			}
		}
	}
	return 0;
}
