#include "system.h"
#include "my_types.h"
#include "configuration.h"
#include "dynamic_var.h"
#include "altera_avalon_timer_regs.h"
#include "altera_avalon_pio_regs.h"

void onTimerInterrupt(void* context);
void onKeyInterrupt(void* context);
void onCollisionInterrupt(void* context);
void onSyncInterrupt(void* context);

void enableCheatMod(){
	ignoringCollision = TRUE;
	enableCollisionInterrupt(FALSE);
	IOWR(CHEAT_MOD_BASE, 0, 1);
}

void disableCheatMod(){
	ignoringCollision = FALSE;
	enableCollisionInterrupt(TRUE);
	IOWR(CHEAT_MOD_BASE, 0, 0);
}

void enableCollisionInterrupt(bool enabled){
	/* Reset register */
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(COLLISION_BASE, 0x0);
	if (enabled) {
		IOWR_ALTERA_AVALON_PIO_IRQ_MASK(COLLISION_BASE, 0x1);
	} else {
		IOWR_ALTERA_AVALON_PIO_IRQ_MASK(COLLISION_BASE, 0x0);
	}
}

int initInterruptions(){

	// KEYS REGISTRATION
	void* edge_capture_ptr = (void*) &edge_capture;
	/* Enable the 4 interrupts. */
	IOWR_ALTERA_AVALON_PIO_IRQ_MASK(KEYS_BASE, 0xF);
	/* Reset the edge_capture register. */
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(KEYS_BASE, 0x0);
	/* Register the interrupt handler. */
	int ISR_keys_success = 0;
	ISR_keys_success = alt_ic_isr_register(KEYS_IRQ_INTERRUPT_CONTROLLER_ID,
			KEYS_IRQ,
			onKeyInterrupt,
			edge_capture_ptr,
			0x0);

	// COLLISION PIO REGISTRATION
	enableCollisionInterrupt(TRUE);
	int ISR_collision_success = 0;
	ISR_collision_success = alt_ic_isr_register(COLLISION_IRQ_INTERRUPT_CONTROLLER_ID,
			COLLISION_IRQ,
			onCollisionInterrupt,
			0x0,
			0x0);

	// SYNC PIO REGISTRATION
	IOWR_ALTERA_AVALON_PIO_IRQ_MASK(IMAGE_SYNC_BASE, 0x1);
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(IMAGE_SYNC_BASE, 0x0);
	int ISR_sync_success = 0;
	ISR_sync_success = alt_ic_isr_register(IMAGE_SYNC_IRQ_INTERRUPT_CONTROLLER_ID,
			IMAGE_SYNC_IRQ,
			onSyncInterrupt,
			0x0,
			0x0);

	// TIMER REGISTRATION
	alt_u16 periodL = 0;
	alt_u16 periodH = 0;
	alt_u32 periodT = 0;

	// stop timer
	IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_0_BASE, ALTERA_AVALON_TIMER_CONTROL_STOP_MSK);

	// clears TO flag
	IOWR_ALTERA_AVALON_TIMER_STATUS(TIMER_0_BASE, 0);

	// Period configuration
	//	printf("Period parameter: %d, (0x%x)\n", TPERIOD, TPERIOD);
	IOWR_ALTERA_AVALON_TIMER_PERIODL(TIMER_0_BASE, (alt_u16)((int)TPERIOD & 0x0000FFFF) );
	IOWR_ALTERA_AVALON_TIMER_PERIODH(TIMER_0_BASE, (alt_u16)((int)TPERIOD >> 16) );
	// ...check the written value
	periodL = IORD_ALTERA_AVALON_TIMER_PERIODL(TIMER_0_BASE);
	periodH = IORD_ALTERA_AVALON_TIMER_PERIODH(TIMER_0_BASE);
	//printf("Configured periodL: %d (0x%x) \n", periodL, periodL);
	//printf("Configured periodH: %d (0x%x) \n", periodH, periodH);
	periodT = (periodH << 16) | periodL;
	//	printf("Configured period readback: %d (0x%x) \n", periodT, periodT);

	// set continuous mode and enable ints
	IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_0_BASE, (ALTERA_AVALON_TIMER_CONTROL_CONT_MSK |
			ALTERA_AVALON_TIMER_CONTROL_ITO_MSK  ));

	// register the timer irq to be serviced by handle_timer_interrupt() function
	//	alt_irq_register(TIMER_IRQ, 0, handle_timer_IRQ);// old API
	int ISRtimer_success = 0;
	ISRtimer_success = alt_ic_isr_register(TIMER_0_IRQ_INTERRUPT_CONTROLLER_ID,
			TIMER_0_IRQ,
			onTimerInterrupt,
			0x0,
			0x0);

	return (ISR_keys_success == 0 && ISR_collision_success == 0 && ISR_sync_success == 0 && ISRtimer_success == 0) ? TRUE : FALSE;
}

/* Timer IRQ handler
 * Uses the enhanced API
 */
void onTimerInterrupt(void* context){
	// clear IRQ status in order to prevent retriggering
	IOWR_ALTERA_AVALON_TIMER_STATUS(TIMER_0_BASE, 0);

	/* Flag IRQ assertion to main() */
	timer_IRQ = TRUE;
}

/* KEY IRQ handler
 * Uses the enhanced API
 */
void onKeyInterrupt(void* context){
	/* Cast context to edge_capture's type. It is important that this be
	 * declared volatile to avoid unwanted compiler optimization */
	volatile int* edge_capture_ptr = (volatile int*) context;
	/* Store the value in the Button's edge_capture register in *context. */
	*edge_capture_ptr = IORD_ALTERA_AVALON_PIO_EDGE_CAP(KEYS_BASE);
	//	/* Reset the Button's edge capture register. */
	//	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_KEY_BASE, 0);
	/* Reset the Button's edge capture register bit set by the IRQ. */
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(KEYS_BASE, edge_capture);
	/*
	 * Read the PIO to delay ISR exit. This is done to prevent a spurious
	 * interrupt in systems with high processor -> pio latency and fast
	 * interrupts */
	IORD_ALTERA_AVALON_PIO_EDGE_CAP(KEYS_BASE);

	/* Flag IRQ assertion to main() */
	key_IRQ = TRUE;
}

void onCollisionInterrupt(void* context){
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(COLLISION_BASE, 0x0);
	/*
	 * Read the PIO to delay ISR exit. This is done to prevent a spurious
	 * interrupt in systems with high processor -> pio latency and fast
	 * interrupts */
	IORD_ALTERA_AVALON_PIO_EDGE_CAP(COLLISION_BASE);

	/* Flag IRQ assertion to main() */
	collision_IRQ = TRUE;
}

void onSyncInterrupt(void* context){
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(IMAGE_SYNC_BASE, 0x0);
	/*
	 * Read the PIO to delay ISR exit. This is done to prevent a spurious
	 * interrupt in systems with high processor -> pio latency and fast
	 * interrupts */
	IORD_ALTERA_AVALON_PIO_EDGE_CAP(IMAGE_SYNC_BASE);

	/* Flag IRQ assertion to main() */
	sync_IRQ = TRUE;
}

