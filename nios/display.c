#include <io.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include "altera_avalon_pio_regs.h" //IOWR_ALTERA_AVALON_PIO_DATA
#include "my_types.h"
#include "dynamic_var.h"
#include "configuration.h"
#include "system.h"

float getGameSpeed();
int getFreeCactusSlotIndex();
int getFreeCloudSlotIndex();
int getFurthestCactusX();
int getFurthestCloudX();

int getRandomNumber(int max)
{
	int value = rand();
	return (value % (max+1));
}

// Makes the dinosaur jumping
void jumpDino()
{
	if (!dinoJumping)
	{
		dinoJumping = TRUE;
		dinoJumpBeginDateMs = timeMs;
	}
}

// Writes the new dinosaur location in the nios output (depending on timeMs)
void displayDino()
{
	int posY = 0;
	if (dinoJumping)
	{
		long timeSinceJumpBegin = timeMs - dinoJumpBeginDateMs;
		if (timeSinceJumpBegin < DINO_JUMP_DURATION_MS)
		{
			//parabolic jump
			float A = -4 * (float)DINO_JUMP_HEIGHT / (float)(DINO_JUMP_DURATION_MS * DINO_JUMP_DURATION_MS);
			float B = 4 * (float)DINO_JUMP_HEIGHT / (float)DINO_JUMP_DURATION_MS;
			posY = A * (timeSinceJumpBegin * timeSinceJumpBegin) + B * timeSinceJumpBegin;
		}
		else
		{
			dinoJumping = FALSE;
		}
	}
	IOWR_ALTERA_AVALON_PIO_DATA(DINO_X_BASE, DINO_POSX);
	IOWR_ALTERA_AVALON_PIO_DATA(DINO_Y_BASE, (WINDOW_HEIGHT - FLOOR_LEVEL - dinoIcon_height - posY));
}

void displayDinoCentered()
{
	IOWR_ALTERA_AVALON_PIO_DATA(DINO_X_BASE, (int)((float)(WINDOW_WIDTH - dinoIcon_width)/2));
	IOWR_ALTERA_AVALON_PIO_DATA(DINO_Y_BASE, (int)((float)(WINDOW_HEIGHT - dinoIcon_height)/2));
}

void hideDino()
{
	//TODO
}

// Writes new catus locations in the nios output (depending on timeMs)
void displayCacti()
{
	// shift all displayed cacti
	for (int i = 0; i < sizeof(cacti)/sizeof(cacti[0]); i++){
		if (cacti[i].visible) {
			cacti[i].posX = (int)(WINDOW_WIDTH + cactusIcon_width - (float)(timeMs - cacti[i].creationTimeMs) / 1000.0 * getGameSpeed());
		}
	}

	// check cacti that exit the window
	for (int i = 0; i < sizeof(cacti)/sizeof(cacti[0]); i++){
		if (cacti[i].posX <= 0) {
			cacti[i].visible = FALSE;
		}
	}

	// get a free slot to display a new cactus
	int freeSlotIndex = getFreeCactusSlotIndex();
	if (freeSlotIndex >= 0) { // If a slot is available
		int furthestX = getFurthestCactusX();
		if (furthestX == -1 /* no cactus displayed */ || (WINDOW_WIDTH - furthestX - cactusIcon_width) > MIN_DIST_BTW_CACTI) {
			//TODO: aleat generation X
			cacti[freeSlotIndex].visible = TRUE;
			cacti[freeSlotIndex].creationTimeMs = timeMs + getRandomNumber(1000);
			cacti[freeSlotIndex].posX = WINDOW_WIDTH + cactusIcon_width;
		}
	}

	// output new locations
	IOWR_ALTERA_AVALON_PIO_DATA(CACTUS_0_BASE, (cacti[0].visible * pow(2, 10) + cacti[0].posX));
	IOWR_ALTERA_AVALON_PIO_DATA(CACTUS_1_BASE, (cacti[1].visible * pow(2, 10) + cacti[1].posX));
	IOWR_ALTERA_AVALON_PIO_DATA(CACTUS_2_BASE, (cacti[2].visible * pow(2, 10) + cacti[2].posX));
}

void hideCacti()
{
	// make all cacti hidden
	for (int i = 0; i < sizeof(cacti)/sizeof(cacti[0]); i++){
		cacti[i].visible = FALSE;
	}
	// write outputs
	IOWR_ALTERA_AVALON_PIO_DATA(CACTUS_0_BASE, 0);
	IOWR_ALTERA_AVALON_PIO_DATA(CACTUS_1_BASE, 0);
	IOWR_ALTERA_AVALON_PIO_DATA(CACTUS_2_BASE, 0);
}

// Writes new cloud locations in the nios output (depending on timeMs)
void displayClouds()
{
	// shift all displayed clouds
	for (int i = 0; i < sizeof(clouds)/sizeof(clouds[0]); i++){
		if (clouds[i].visible) {
			clouds[i].posX = (int)(WINDOW_WIDTH + cloudIcon_width - (float)(timeMs - clouds[i].creationTimeMs) / 1000.0 * getGameSpeed() / (float)CACTUS_TO_CLOUD_SPEED_RATIO);
		}
	}

	// check clouds that exit the window
	for (int i = 0; i < sizeof(clouds)/sizeof(clouds[0]); i++){
		if (clouds[i].posX <= 0) {
			clouds[i].visible = FALSE;
		}
	}

	// get a free slot to display a new cloud
	int freeSlotIndex = getFreeCloudSlotIndex();
	if (freeSlotIndex >= 0) { // If a slot is available
		int furthestX = getFurthestCloudX();
		if (furthestX == -1 /* no cloud displayed */ || (WINDOW_WIDTH - furthestX - cloudIcon_width) > MIN_DIST_BTW_CLOUDS) {
			clouds[freeSlotIndex].visible = TRUE;
			clouds[freeSlotIndex].creationTimeMs = timeMs + getRandomNumber(1000);
			clouds[freeSlotIndex].posX = WINDOW_WIDTH + cloudIcon_width;
			clouds[freeSlotIndex].posY = getRandomNumber(2 * AVERAGE_CLOUD_LEVEL);
		}
	}

	// output new locations
	IOWR_ALTERA_AVALON_PIO_DATA(CLOUD_0_BASE, (clouds[0].visible * pow(2, 20) + clouds[0].posX * pow(2, 10) + clouds[0].posY));
	IOWR_ALTERA_AVALON_PIO_DATA(CLOUD_1_BASE, (clouds[1].visible * pow(2, 20) + clouds[1].posX * pow(2, 10) + clouds[1].posY));
	IOWR_ALTERA_AVALON_PIO_DATA(CLOUD_2_BASE, (clouds[2].visible * pow(2, 20) + clouds[2].posX * pow(2, 10) + clouds[2].posY));
}

void hideClouds()
{
	// make all clouds hidden
	for (int i = 0; i < sizeof(clouds)/sizeof(clouds[0]); i++){
		clouds[i].visible = FALSE;
	}
	// write outputs
	IOWR_ALTERA_AVALON_PIO_DATA(CLOUD_0_BASE, 0);
	IOWR_ALTERA_AVALON_PIO_DATA(CLOUD_1_BASE, 0);
	IOWR_ALTERA_AVALON_PIO_DATA(CLOUD_2_BASE, 0);
}

void displayScore(int score){
	IOWR_ALTERA_AVALON_PIO_DATA(SCORE_BASE, score);
}

float getGameSpeed(){
	float output;
	if (timeMs < GAME_SPEED_RAMP_DURATION_S * 1000){
		output = (float)MIN_GAME_SPEED + (float)(MAX_GAME_SPEED - MIN_GAME_SPEED) / (GAME_SPEED_RAMP_DURATION_S * 1000) * timeMs;
	}else{
		output = (float)MAX_GAME_SPEED;
	}
	return output;
}

int getFreeCactusSlotIndex(){
	int i = 0;
	while( i < sizeof(cacti)/sizeof(cacti[0])){
		if (!cacti[i].visible){
			return i;
		}
		i++;
	}
	return -1;
}

int getFreeCloudSlotIndex(){
	int i = 0;
	while( i < sizeof(clouds)/sizeof(clouds[0])){
		if (!clouds[i].visible){
			return i;
		}
		i++;
	}
	return -1;
}

int getFurthestCactusX(){
	int maxX = -1;
	for (int i = 0; i < sizeof(cacti)/sizeof(cacti[0]); i++){
		if (cacti[i].visible && cacti[i].posX > maxX) {
			maxX = cacti[i].posX;
		}
	}
	return maxX;
}

int getFurthestCloudX(){
	int maxX = -1;
	for (int i = 0; i < sizeof(clouds)/sizeof(clouds[0]); i++){
		if (clouds[i].visible && clouds[i].posX > maxX) {
			maxX = clouds[i].posX;
		}
	}
	return maxX;
}

/**
 * The value must be between 0 and 10
 */
void displayLevel(int value)
{
	if(value <= 8){
		IOWR(LIFE_BASE, 0, value);
	}else{
		IOWR(LIFE_BASE, 0, 8);
	}
}
