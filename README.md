# Dinosaure

VHDL and C project for the implementation of the chrome dino game on a FPGA

# Content

## Assets
- python script for png -> .hex file conversion
- png files
- hex files

## Web poc
Web proof of concept used for the design of a C structure

## Nios
Code embedded in the nios CPU

# VHDL
*Empty*