import re
import sys

import cv2


def getFileName(str):
    matchs = re.search("(.+)\.(.+)", str)
    return matchs.group(1)


BYTE_COUNT = 4


# Read file
print('Input png Filename: ')
input = input()
name = getFileName(input)
img = cv2.imread(input, cv2.IMREAD_UNCHANGED)
# Reset hex file if existing, create it otherwise
reset = open(("%s.hex" % name), "wb")
reset.seek(0)
reset.close()

# Write data in the hex file
with open(("%s.hex" % name), "a") as output:
    address = 0
    for row in img:
        for pixel in row:

            transparent = False
            # Make the pixel transparent if its alpha channel is greater than 50%
            if len(pixel) == 4:
                transparent = (pixel[3] < 50)
            
            # Calculate checksum
            checksum = (address & 0x00ff) + ((address & 0xff00)
                                             >> 8) + BYTE_COUNT + (1 if transparent else 0) + sum(pixel[0:3])
            # Only watch the lsB
            checksum &= 0xFF
            checksum = (~checksum + 1) & 0xff
            
            # Write the data/metadata corresponding to the pixel
            output.write(
                ":{byte_count:02x}{address:04x}{type:02x}{red:02x}{green:02x}{blue:02x}{transparency:02x}{checksum:02x}\r\n".format(
                    **{
                        'byte_count': BYTE_COUNT,
                        'address': address,
                        'type': 0,
                        'red': pixel[2],
                        'green': pixel[1],
                        'blue': pixel[0],
                        'transparency': 1 if transparent else 0,
                        'checksum': checksum
                    }
                ).upper()
            )
            address += 1
    # Add end of file instruction
    output.write(":00000001FF")
    print('Done! created a {address} pixels hex file'.format(address=address))
